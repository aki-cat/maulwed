
# Plot changes

> First of all, check the professor's movie recs

**Backstory**

Tyche is intensely in love with Hasha. She has always been. Hasha on the other hand has had multiple failed romances over the years. Mostly with boys, one one-sided thing with Olivia in middle school, and one fling with a customer who moved away in a couple of months and they grew apart. Tyche was never the jealous type, though. She enjoyed Hasha's flings as friends, because she was very lonely and had not friends other than Hasha. So having other people to accompany them in Hasha's adventures was a treat. Helen sometimes filled that role, but she was an adult and had a little of that authority thing.

**Mane's Story**

Mane is a college professor who lost his own kid. Spoilers: his kid became Wild two years ago, when he took one year sabbatical from his job. Now he's become a lil' cray cray and he been stalking Helen, and then he actually got into her. He became conviced it was because of Hasha – circumstantial evidence. His kid became Wild in that neighbourhood. There is like a neighbourhood curse. Every so often there's like a werewolf tale thing going on. Like, people say sometimes people go crazy in that park on full moons or whatever. Stupid. It's when Hasha or Tyche suffer a great heartbreak. Idiots.

So Mane tries to turn people to "study". He poisoned Helen lightly to get Hasha busy and not go into the park at night (Helen usually works the night shift!), but she goes anyway because of what Tyche told her the day before. Then she gets attacked by the coyote, and Mane is caught red handed and he's like, oh shit. He says something about her mom ("You don't want to end like your mother!") but she's just lightly poisoned! "What have you **done** to mom?!?!?!" And oh boy I don't wanna be him at that time. Mauled to death. Wild tigre on the loose. Wow. That all happens on first cycle, and any cycle in which Hasha stays unsupervised. If unsupervised, Tyche returns to the shop to notice she's gone, and she's mauled when she exits (she's either chased or ambushed by Hash after... 11pm?). Or she's mauled on the way to Hasha's, if she goes through the park (if she was last with R&D she takes that route). This is just the standard cycle. If you do a chapter that is not the first cycle, this changes drastically.


**Prologue**

On her way back home from work, Ty is attacked by a Wild coyote! She escapes, barely, and gets home freaked out. It lurked by the park near Hasha's place, but she manages to climb a van and mace its face. It runs away.

Getting home, she finds her door unlocked and Hasha is there drinking beer. Ty is shook, and tells her about the coyote. Hasha not only believes her but becomes INTENSELY curious about it. An Animal gone Wild? They had to check it out before the authorities found it! Ty wants to go on the adventure, but she is SCARED. She just wants to curl up with Hasha and sleep. They do just that, at least for that night.

**The first cycle**

REQUIREMENTS:
+ None. Mandatory.

The next day, they go to Hash's place and she's like, LETS LOOK FOR THE COYOTE TONIGHT! Ty meets Derek on the market while getting food for Hash, blabla; Ty is scared and excited at the same time. She reluctantly says yes. They go out at 6pm, closing shop early. Hasha just dashes out to the park and looks around the place. Ty follows, only to find Hasha arguing with a lion who is telling her to go home.

"Ma'am, go home tonight, it is not safe."

"You can't tell me what to do."

So he grabs at her arm and get a clawful to the face. Ty has her mace ready.

"LISTEN. It's dangerous out here tonight! Just go home!"

"LEMME GO!"

And HE starts convulsing and roaring, and Hasha is in danger and Ty just pushes her away and gets bitten. Hasha then TFs into Wild forme and kills the guy. Ty is scared and tries to talk to Hasha. She gets bit. She wakes up.

NOTES:
Jessica is a delinquent teenage hyena who is looking for her big brother. She is at the park, seemingly loitering, but she's actually looking for clues. About her brother, a COYOTE WHOS GONE MISSING. In a "i don't want to talk to people ugh" kind of way, plus its only been a day or whatever.

So the variations of this are:

**The trouble lovers**

REQUIREMENTS:
+ First cycle.
+ Ty meets Derek alone at the market. Visits him before 6pm.

Tyche visits Derek and Rick. They insist on going out too. Ty gets dragged off and questions her choice in friends. They have a lovely adventure until they are attacked by the coyote! They manage to run from it, and they hide until late, and they talk while hiding and shivering with fear. Eventually they go out and it's gone! So Tyche goes back and...

And meets Hasha, Wild, on the park. The is alone now, and she doesn't get a chance to run fast enough. She is bit. Wakes Up.

ALTERNATIVELY: The play
Tyche visits Derek and Rick. They rehearse a play of a fantasy story based on the origin of all animals, because Rick is on that play. He manages to drag Derek as well. Tyche plays as Ani, the dragon, and Rick plays as Ina, the white tigre (Ani's costume is easy, it's just wings, Ina's is a lil trickier, but a striped blouse might be enough). Derek plays as a minor role, as the friend who kills all dragons to defend Ina when they rage war. Then Rick plays the whole "Oh my dearest, I never meant for thee to Die!" And Tyche has to say "I love you" and die. She blushes and stutters. (Remember your line minigame?)

And and and and Rick goes to compliment her like, that shy ilu was so CUTE! It would be a killer move on any straight guy. And Derek goes like "I don't think objectifying her shyness is a good thing..." And Tyche goes "I-I'm Not Shy!! I just don't like saying it. It's um, not something you say, they just have to Know. I think" And they go, "Oh dear, if that were the case, Derek and I would never have hooked up. You should tell people how you feel!"

"Yeah, Ty!" says Derek, "I think it was that friend you introduced us that got me the courage to ask Rick out."

"What, Hasha??"

"Yeah that tigre!"

**The sleepover**

REQUIREMENTS:
+ First cycle.
+ Do not go to Derek's.

This time, Tyche convinces Hasha to not go out. Tyche bakes to convince Hasha. (Baking minigame?). So they stay in Hash's room and Hasha tells a scary story (the mythology!). Hasha bugs Ty about how the goddesses are in love, and they fight because Ty is unable to voice her feelings. Ty feels HEARTBREAK. Then they hear a window break downstairs. It is the lion from the park! It attacks Ty and Hasha comes to the rescue! Only...Hash goes Wild again! Bit, wake up.

**The drunkards**

REQUIREMENTS:
+ First cycle.
+ Meet Derek at the Market with Hasha.

Derek and Rick come over and bring drinks (Hasha needs to come to the market)! They go out to the park despite Ty's protests. They get in time to see the lion capturing the coyote. They panic when they are found, and they just run. The guy chases them, but in mid chase he goes Wild! 

Hasha stops to fight him. She can take him, she says. And they roll around until she is Wild herself and has killed him. Derek and Rick are gone. Tyche is alone. She runs but it's too late.

**The resolve**

REQUIREMENTS:
+ First cycle.
+ Meet Derek at the Market with Hasha.
+ Do not lie to Hasha when she asks what's wrong.
+ Done all other endings.

When Tyche admits she would just rather be with Hasha all day long, they become increasingly happy! They go to the Rick&Derek's place and Ty tells them about the coyote. They all grow curious, and tell Ty how they've seen other Wild animals too! And that there's been disappearances and shit.

They decide to go out to the bar instead of going to the park. They see the wild coyote running wild. Of course, everyone runs out to look at the scene. They see the coyote being caged by the lion, but they all hide back inside the bar this time. Rick records this. The lion gets inside and talks to everyone.

"Hey, kids. Look, we paying you good money to not talk about this to anyone."

Rick is going, yeah, tell that to the internet. That guy went off the rails and you just cage him?

He gets mad, tries to swipe Rick's phone. Derek pushes him with enough force for him to fall back. "Don't touch my boyfriend!"

The guy is insistent "You don't understand! We don't know why this is happening!"

Hasha goes, "Yeah, well, I'm gonna check the coyote out."

"No, don't!"

He tries to hold her back, but she is strong. They end up fighting until he BITES.

"OW, hey, you're really gonna bite!?"

But he has gone WILD! Inside the bar! And he jumps Hash. Derek gets a chair on him so he passes out, and they RUN.

They hide back in the canids' place, which is right next door. They look out the window and see another van come to get the lion. They drive away.

Tyche holds on to Hash, and the canids hold on to each other. The morning comes. It is over. But what happened?

They all huddle and talk. Hasha goes, "You guys got together after what I said?"

"Yeah, we were being dumbasses. Like you two are."

"We're just–"

"Sure, whatever you say, Ty."

When they are all asleep, Hasha speaks.

"I was really scared."

"You're never scared."

"I'm always scared, Ty. Of everything. Ever since Olivia. But this time, even as I was almost killed, I was scared for you."

"I'm okay now."

"Yeah, but... I guess I'm selfish. I was scared of not having you around, really."

"Hash..."

Hash kisses Ty. "Really scared I would never be able to do this."

"You dork."

They kiss again. And then they fall asleep on Rick and Derek's couch.

But what happened to the Wild Animals? No one knows... but it didn't happen again for a long time.


