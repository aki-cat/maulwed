
extends Node2D

onready var input_ev = get_node("input_ev")

func _input(ev):
	input_ev.set_text(ev.as_text())
