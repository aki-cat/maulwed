tool
extends AnimationPlayer

func init_animation(anim_name):
  var anim
  if not has_animation(anim_name):
    anim = Animation.new()
    add_animation(anim_name, anim)
  else:
    anim = get_animation(anim_name)
  anim.set_loop(true)

func _enter_tree():
  init_animation('idle')
  init_animation('run')
  init_animation('walk')
