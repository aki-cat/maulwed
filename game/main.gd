# MAIN
extends Node2D

const PACK = preload("res://common/pack.gd")
const FADE_TIME = 0.5

export(String) var first_state

onready var music = get_node("/root/music")
onready var fader = get_node("effects/fader")
onready var waiter = get_node("effects/waiter")
onready var gamestate = get_node('state')

var states = PACK.new("states")

func _ready():
	switch_to(first_state)

func switch_to(statename):
	var current
	if gamestate.get_child_count() > 0:
		current = gamestate.get_child(0)
		current.leave()
	get_tree().paused = true
	fader.fade_out(FADE_TIME)
	music.stop(FADE_TIME)
	yield(fader, "faded_out")
	waiter.wait_for(FADE_TIME)
	yield(waiter, "timeout")
	get_tree().paused = false
	if current != null:
		current.free()
	var newstate = states.instance(statename)
	gamestate.add_child(newstate)
	newstate.connect("requested_state_switch", self, "switch_to",
			[], CONNECT_ONESHOT)
	newstate.connect("requested_game_quit", self, "quit",
			[], CONNECT_ONESHOT)
	fader.fade_in(FADE_TIME)
	yield(fader, "faded_in")
	newstate.enter()

func get_state():
	return get_node("state").get_child(0)

# On Quit

func quit():
	get_tree().paused = true
	music.stop(FADE_TIME)
	fader.fade_out(FADE_TIME)
	yield(fader, "faded_out")
	waiter.wait_for(FADE_TIME)
	yield(waiter, "timeout")
	get_tree().quit()

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		quit()

func _exit_tree():
	states.free()
