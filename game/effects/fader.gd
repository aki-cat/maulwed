
extends ColorRect

signal faded_out
signal faded_in

const BLACK = Color(0x101010ff)
const TRANSP = Color(0x10101000)

onready var tween = get_node("tween")

func _ready():
	fade_in(0.25)

func fade_out(s):
	tween.stop_all()
	tween.interpolate_property(self, "color", TRANSP, BLACK, s,
			Tween.TRANS_LINEAR, Tween.EASE_IN_OUT, 0)
	tween.start()
	yield(tween, "tween_completed")
	emit_signal("faded_out")

func fade_in(s):
	tween.stop_all()
	tween.interpolate_property(self, "color", BLACK, TRANSP, s,
			Tween.TRANS_LINEAR, Tween.EASE_IN_OUT, 0)
	tween.start()
	yield(tween, "tween_completed")
	emit_signal("faded_in")

