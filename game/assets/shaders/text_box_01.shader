shader_type canvas_item;

uniform vec2 DIM = vec2(128, 128);
uniform float CORNER = 1.0;

float dist(vec2 u, vec2 v) {
	return max(abs(u.x - v.x), abs(u.y - v.y));
}

void fragment() {
	vec2 lu, ld, ru, rd;
	lu = vec2(0, 0);
	ld = vec2(0, DIM.y);
	ru = vec2(DIM.x, 0);
	rd = DIM;
	float visible = 1.0;
	visible = min(visible, step(CORNER, dist(lu, UV)));
	visible = min(visible, step(CORNER, dist(ld, UV)));
	visible = min(visible, step(CORNER, dist(ru, UV)));
	visible = min(visible, step(CORNER, dist(rd, UV)));
	COLOR.a = visible;
}
