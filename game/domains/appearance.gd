
extends Node2D

const PACK = preload("res://common/pack.gd")
const ANIMATIONS = ['idle', 'walk', 'run']
const FLIPS = [1, -1]

enum { STATE_IDLE, STATE_WALK, STATE_RUN }
enum { FLIP_LEFT = 1, FLIP_RIGHT = -1 }

# Current animation info
onready var sprite = get_node("sprite")

var sprites = PACK.new("domains/sprites")
var state = STATE_IDLE

func _ready():
	play()

func _exit_tree():
	sprites.free()

# Animation Controls

func stop():
	if sprite.has_node('animations'):
		var animations = sprite.get_node('animations')
		animations.stop()

func play():
	if sprite.has_node('animations'):
		var animations = sprite.get_node('animations')
		animations.emit_signal("animation_changed", animations.current_animation, ANIMATIONS[state])
		animations.play(ANIMATIONS[state])

func play_custom(animation_name):
	if sprite.has_node('animations'):
		var animations = sprite.get_node('animations')
		animations.emit_signal("animation_changed", animations.current_animation, animation_name)
		animations.play(animation_name)

# Animation Setup

func set_sprite(sprite_name):
	remove_child(sprite)
	sprite.queue_free()
	sprite = sprites.instance(sprite_name)
	sprite.set_name("sprite")
	add_child(sprite)
	set_state(state)

func set_flip(flip):
	assert(flip in FLIPS)
	scale.x = flip

func get_flip():
	return scale.x

func set_state(st):
	assert(st >= STATE_IDLE and st <= STATE_RUN)
	state = st
	play()

func get_state():
	return state

func set_idle_state():
	set_state(STATE_IDLE)

func set_walk_state():
	set_state(STATE_WALK)

func set_run_state():
	set_state(STATE_RUN)


# Save & Load

func save():
	var to = {}
	to.state = state
	to.flipped = scale.x
	return to

func load(from):
	state = from.state
	scale.x = from.flipped
	set_state(state)

