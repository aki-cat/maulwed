
extends "res://domains/component.gd"

const ConditionParser = preload("res://common/condition_parser.gd")

export(String, MULTILINE) var conditional_statements = ""
export(PoolStringArray) var conditions = []
export(bool) var time_based = false
export(float, 0, 24, 0.01) var time_window_start = 0
export(float, 0, 24, 0.01) var time_window_end = 0

var parsed_conditions

func _ready():
	if conditions == null:
		conditions = []
	if time_based:
		var window_start = "st.time >= %d" % int(time_window_start * 60 * 60)
		var window_end = "st.time < %d" % int(time_window_end * 60 * 60)
		conditions.append(window_start)
		conditions.append(window_end)
	var gamedata = get_node("/root/profile").current_save
	var conditions_extended = conditional_statements.split("\n")
	var all_conditions = Array(conditions) + Array(conditions_extended)
	parsed_conditions = ConditionParser.parse(all_conditions, gamedata)

func are_conditions_met():
	for condition in parsed_conditions:
		if !condition.check():
			return false
	return true
