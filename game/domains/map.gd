
extends Node2D

const CAM = preload("res://common/cam.tscn")
const PLAYER = preload("res://domains/bodies/player.tscn")

signal map_paused
signal map_resumed
signal map_changed
signal requested_quit

export(String) var map_name = "Unknown"
export(bool) var time_flow = false
export(bool) var allow_running = false
export(AudioStream) var bgm

onready var scenes = get_node("scenes")
onready var behaviours = get_node("behaviours")
onready var camera = get_node("cam")
onready var se = get_node("/root/se")

var player
var next_map
var is_bgm_playing = true
var bodies setget ,get_bodies
var paused = true
var locked = false
var interactables = []
var stepables = []

# Specials

func _ready():
	camera.set_map(self)
	camera.make_current()

func _enter_tree():
	add_to_group("map")

func _exit_tree():
	remove_from_group("map")

func _physics_process(dt):
	if not paused and not locked:
		if next_map != null:
			pause()
			return emit_signal("map_changed", next_map[0], next_map[1])
		if not check_auto_trigger():
			check_step_trigger()


# Setups

func enter(hud, spawn):
	if is_bgm_playing and bgm:
		get_node("/root/music").play(bgm, 1)
	set_name("map")
	setup_bodies()
	set_player(spawn)
	run()

func leave():
	pause()
	unset_player()

func setup_bodies():
	for body in get_bodies():
		setup_body_triggers(body)

func setup_body_triggers(body):
	var area = body.get_interaction_area()
	area.connect("body_entered", self, "add_stepable", [body])
	area.connect("body_exited", self, "remove_stepable", [body])
	area.connect("area_entered", self, "add_interactable", [body])
	area.connect("area_exited", self, "remove_interactable", [body])

func run():
	if paused and not locked:
		paused = false
		emit_signal("map_resumed")
		camera.set_focus(player)
		behaviours.resume()
		update_interactable_bodies()

func pause():
	if not paused:
		paused = true
		emit_signal("map_paused")
		behaviours.pause()
		deactivate_interactable_bodies()

func lock():
	locked = true

func is_paused():
	return paused

func is_locked():
	return locked

func quit():
	lock()
	emit_signal("requested_quit")

func set_player(spot_name):
	var gamedata = get_node("/root/profile").current_save
	player = PLAYER.instance()
	get_node("bodies").add_child(player)
	gamedata.load_player_data(player)
	# setup player position in map
	set_body_in_spawn(player, spot_name)
	if allow_running:
		player.set_speed(player.SPEED_FAST)
	else:
		player.set_speed(player.SPEED_MID)
	# setup camera
	camera.set_focus(player)
	

func unset_player():
	var gamedata = get_node("/root/profile").current_save
	gamedata.save_player_data(player)
	get_node("bodies").remove_child(player)
	player.queue_free()

func get_player():
	return player

# Stepables

func add_stepable(player_body, body):
	if player_body != player:
		return
	if not (body in stepables):
		stepables.append(body)


func remove_stepable(player_body, body):
	if player_body != player:
		return
	var idx = stepables.find(body)
	if idx != -1:
		stepables.remove(idx)

# Interactables

func add_interactable(area, body):
	if body == player:
		return
	if interactables.find(body) == -1:
		interactables.push_front(body)
		update_interactable_bodies()

func remove_interactable(area, body):
	if body == player:
		return
	var idx = interactables.find(body)
	if idx != -1:
		body.hide_notification()
		interactables.remove(idx)
		update_interactable_bodies()

func update_interactable_bodies():
	if paused:
		return
	var available = false
	for body in interactables:
		if !available and !body.is_hidden() and body.has_interactable_scenes():
			body.show_notification()
			available = true
		else:
			body.hide_notification()

func deactivate_interactable_bodies():
	for body in interactables:
		body.hide_notification()


# Scene Triggers Checkers

func check_step_trigger():
	for body in stepables:
		if not body.is_hidden() and body.has_scenes():
			var body_scenes = body.get_scenes()
			if body_scenes.trigger_step():
				print("scene triggered by step!")
				return true
	return false

func check_interact_trigger():
	if not player:
		print("WARNING: NO PLAYER!")
		return 
	for body in interactables:
		if !body.is_hidden() and body.has_interactable_scenes():
			var body_scenes = body.get_scenes()
			if not body.is_animation_locked():
				body.look_at(player)
			player.look_at(body)
			if body_scenes.trigger_interact():
				se.play("interact")
				camera.set_focus(body)
				print("scene triggered by interaction!")
			body.hide_notification()
			break

func check_auto_trigger():
	behaviours.check()
	if scenes.trigger_auto():
		print("scene triggered automatically!")
		return true
	return false

# Getters and Setters

func get_map_name():
	return map_name

func get_spawn_point(spawn_name):
	var nodepath = "spawns/%s" % spawn_name
	if has_node(nodepath):
		return get_node(nodepath)

func set_next_map(map_id, spawn_point):
	next_map = [map_id, spawn_point]

func has_body(body_name):
	for body in get_bodies():
		if body.get_name() == body_name:
			return true
	return false

func get_body(body_name):
	for body in get_bodies():
		if body.get_name() == body_name:
			return body

func get_bodies():
	return get_tree().get_nodes_in_group("bodies")

func does_time_flow():
	return time_flow

func set_body_in_spawn(body, spawn_name):
	var spawn_point = get_spawn_point(spawn_name)
	body.set_global_position(spawn_point.get_global_position())
	body.set_flip(spawn_point.get_direction())


# Save & Load State

func save():
	print("saving map: ", map_name)
	var to = {}
	to.is_bgm_playing = is_bgm_playing
	to.allow_running = allow_running
	to.time_flow = time_flow
	to.scenes = scenes.save()
	to.bodies = {}
	for body in get_bodies():
		if body != player:
			to.bodies[body.name] = body.save()
	return to

func load(from):
	print("loading map: ", map_name)
	if from.has('is_bgm_playing'):
		is_bgm_playing = from.is_bgm_playing
	allow_running = from.allow_running
	time_flow = from.time_flow
	scenes.load(from.scenes)
	var loaded_bodies = {}
	for body in get_bodies():
		loaded_bodies[body.get_name()] = body
	for body_name in from.bodies.keys():
		if loaded_bodies.has(body_name):
			var body = loaded_bodies[body_name]
			if body != player:
				body.load(from.bodies[body_name])
