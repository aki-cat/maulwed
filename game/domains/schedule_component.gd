
extends "res://domains/component.gd"

const EPSILON = 0.01

export(float, 0.0, 24.0, 0.001) var time_window_start = 11.0
export(float, 0.0, 24.0, 0.001) var time_window_end = 12.0

onready var gamedata = get_node("/root/profile").current_save

# Special

func _physics_process(dt):
	step()

func resume():
	setup()
	.resume()
	
# Getter & Setter

func get_progress():
	var seconds = get_time()
	var current_time = seconds / 3600.0 - time_window_start
	var interval = time_window_end - time_window_start
	return max(0, min(1, current_time / interval))

func get_time():
	return gamedata.get_state("time")

func get_game_speed():
	return gamedata.get_persist("game_speed")

# Start, Execute, Finish

func setup():
	pass

func step():
	pass

func wrap_up():
	pass
