
extends TextureRect

const COLORS = preload("res://common/colors.gd")

onready var tween = get_node("tween")

func _enter_tree():
	modulate = COLORS.TRANSP

func fade_in(secs):
	tween.stop_all()
	tween.interpolate_property(self, "modulate",
			COLORS.TRANSP, COLORS.OPAQUE, secs, Tween.TRANS_LINEAR, Tween.EASE_IN, 0)
	tween.start()

func fade_out(secs):
	tween.stop_all()
	tween.interpolate_property(self, "modulate",
			COLORS.OPAQUE, COLORS.TRANSP, secs, Tween.TRANS_LINEAR, Tween.EASE_OUT, 0)
	tween.start()
	yield(tween, "tween_completed")
	queue_free()
