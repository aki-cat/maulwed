
extends "res://domains/component.gd"

const STATIC_OP = preload("res://common/static_op.gd")

enum { STATE_FLAG, PERSIST_FLAG }
const OPS = ["Set", "Add", "Sub", "Mul", "Div", "Mod"]

export(int, "state", "persist") var flag_type = 0
export(String) var flag_name
export(int, "set", "add", "sub", "mul", "div", "mod") var operation_type = 0
export(int, -999999, 999999) var value = 0

onready var gamedata = get_node("/root/profile").current_save

func run(map, hud):
	assert(flag_name != null)
	if flag_type == STATE_FLAG:
		var prev = gamedata.get_state(flag_name)
		var static_operation = STATIC_OP[OPS[operation_type]]
		gamedata.set_state(flag_name, static_operation.exec(prev, value))
	elif flag_type == PERSIST_FLAG:
		var prev = gamedata.get_persist(flag_name)
		var static_operation = STATIC_OP[OPS[operation_type]]
		gamedata.set_persist(flag_name, static_operation.exec(prev, value))
	emit_signal("execution_done")

