
extends "res://domains/component.gd"

export(String) var body_name = ""
export(int) var new_z_index = 0

var body

func run(map, hud):
	assert(map.has_body(body_name))
	body = map.get_body(body_name)
	body.z_index += new_z_index
	emit_signal("execution_done")
