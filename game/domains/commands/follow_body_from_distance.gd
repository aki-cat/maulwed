
extends "res://domains/component.gd"

const MAX_DIST = 96
const MIN_DIST = 320

export(String) var stalker = ""
export(String) var stalkee = ""
var stalker_body
var stalkee_body


func run(map, hud):
	assert(map.has_body(stalker))
	assert(map.has_body(stalkee))
	stalker_body = map.get_body(stalker)
	stalkee_body = map.get_body(stalkee)
	yield(get_tree(), "physics_frame")
	set_physics_process(true)

func _physics_process(delta):
	var x = (stalkee_body.get_global_position() - stalker_body.get_global_position()).x
	if x < -MAX_DIST:
		if x < -MIN_DIST:
			stalker_body.global_position.x += x + MIN_DIST
		stalker_body.walk(stalker_body.DIR_LEFT)
	elif x > MAX_DIST:
		if x > MIN_DIST:
			stalker_body.global_position.x += x - MIN_DIST
		stalker_body.walk(stalker_body.DIR_RIGHT)
	else:
		set_physics_process(false)
		emit_signal("execution_done")
	
