
extends "res://domains/component.gd"

func run(map, hud):
	map.camera.set_focus(null)
	emit_signal("execution_done")
