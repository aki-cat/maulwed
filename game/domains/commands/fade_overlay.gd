
extends "res://domains/component.gd"

export(Color) var color
export(float, EXP, 0, 256, 0.1) var seconds = 1
export(float, EXP, 0, 256, 0.1) var delay = 0
export(bool) var wait = false

var timer = Timer.new()

func _ready():
	assert(color != null)
	if not has_node("timer") and delay > 0:
		add_child(timer)
		timer.set_name("timer")

func _exit_tree():
	timer.free()

func get_overlay(hud):
	return hud.overlay

func run(map, hud):
	var overlay = get_overlay(hud)
	if not wait:
		emit_signal("execution_done")
	if delay > 0:
		timer.set_wait_time(delay)
		timer.start()
		yield(timer, "timeout")
	overlay.connect("fade_done", self, "stop", [map, hud], CONNECT_ONESHOT)
	overlay.fade_to(color, seconds)
