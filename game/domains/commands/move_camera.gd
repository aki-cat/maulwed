
extends "res://domains/component.gd"

export(bool) var reset = false
export(float, EXP, 0.0, 10, 0.1) var time = 1.0

var timer

func _enter_tree():
	timer = Timer.new()
	add_child(timer)

func _exit_tree():
	timer.free()

func run(map, hud):
	var camera = map.camera
	if reset:
		camera.set_focus(map.player)
	else:
		var target = get_node("target")
		assert(target)
		camera.set_focus(target)
	if time > 0:
		timer.set_wait_time(time)
		timer.start()
		yield(timer, "timeout")
	emit_signal("execution_done")

