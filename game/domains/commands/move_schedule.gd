
extends "res://domains/schedule_component.gd"

const DEFS = preload("res://domains/definitions.gd")

export(String) var body_name = ""

onready var start = get_node("start")
onready var end = get_node("end")

var body
var trajectory
var original_body_speed

# Start and Finish

func run(map, hud):
	assert(map.has_body(body_name) and start and end)
	body = map.get_body(body_name)
	original_body_speed = body.speed
	set_trajectory()
	set_body_position()
	play_processes()
	setup()

func setup():
	print("running MoveSchedule of [%s]" % body.get_name())
	step()

func step():
	var spd = get_target_speed()
	body.set_speed(spd)
	if trajectory.x > 0:
		body.walk(body.DIR_RIGHT)
	elif trajectory.x < 0:
		body.walk(body.DIR_LEFT)
	var progress = get_progress()
	if progress >= 1:
		wrap_up()
	

func wrap_up():
	body.set_speed(original_body_speed)
	stop_processes()
	emit_signal("execution_done")

# Setters & Getters

func set_trajectory():
	var start_pos = start.get_global_position()
	var end_pos = end.get_global_position()
	trajectory = (end_pos - start_pos)

func set_body_position():
	var progress = get_progress()
	var start_pos = start.get_global_position()
	body.set_global_position(start_pos + trajectory * progress)
	body.look_at(end)

func get_target_speed():
	var remaining_distance = end.get_global_position()-body.get_global_position()
	var remaining_time = time_window_end * 3600 - get_time()
	var game_speed = get_game_speed()
	if abs(remaining_time) <= EPSILON or game_speed == 0:
			set_body_position()
			return body.SPEED_SLOW
	remaining_time /= DEFS.SECOND_PER_SECOND * game_speed
	var target_spd = (remaining_distance/remaining_time/body.MOVEMENT_SIZE).abs()
	var spd = max(-body.BASE_SPEED, target_spd.x - body.BASE_SPEED)
	spd /= body.SPEED_UNIT
	return spd
