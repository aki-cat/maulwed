
extends "res://domains/component.gd"

export(float, EXP, 0.01, 4096, 0.01) var time = 1

var count = 0.0

func _ready():
	set_process(false)

func run(map, hud):
	count = 0.0
	set_process(true)

func _process(delta):
	count += delta
	if count >= time:
		set_process(false)
		emit_signal("execution_done")
