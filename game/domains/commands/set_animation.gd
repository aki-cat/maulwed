
extends "res://domains/component.gd"

enum { STATE_IDLE, STATE_WALK, STATE_RUN }

export(String) var body_name = ""
export(int, "idle", "walk", "run") var animation_state = STATE_IDLE
export(bool) var use_custom = false
export(String) var custom_animation_name = ""

func run(map, hud):
	assert(map.has_body(body_name))
	var body = map.get_body(body_name)
	var appearance = body.appearance
	if use_custom:
		appearance.play_custom(custom_animation_name)
	else:
		match animation_state:
			STATE_IDLE:
				body.set_idle_state()
			STATE_WALK:
				body.set_walk_state()
			STATE_RUN:
				body.set_run_state()
	emit_signal("execution_done")
