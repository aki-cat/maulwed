
extends "res://domains/schedule_component.gd"

enum { FADE_IN, FADE_OUT }

export(String) var body_name = ""
export(int, "fade_in", "fade_out") var fade_type

var body
var start_alpha
var trajectory

func run(map, hud):
	assert(map.has_body(body_name))
	body = map.get_body(body_name)
	play_processes()
	setup()

func setup():
	print("running cmd [%s]" % get_name())
	body.set_hidden(false)
	match fade_type:
		FADE_IN:
			start_alpha = 0
			trajectory = 1
		FADE_OUT:
			start_alpha = 1
			trajectory = -1
	step()

func step():
	var progress = get_progress()
	body.modulate.a = start_alpha + progress * trajectory
	if progress >= 1:
		wrap_up()

func wrap_up():
	if fade_type == FADE_OUT:
		body.set_hidden(true)
	stop_processes()
	emit_signal("execution_done")
