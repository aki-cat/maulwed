
extends "res://domains/component.gd"

const TARGET_NAME = "target"

export(String) var body_name = ""
export(bool) var use_spawn_point = false
export(String) var target_spawn = "initial"

func run(map, hud):
	assert(map.has_body(body_name))
	var body = map.get_body(body_name)
	if use_spawn_point:
		map.set_body_in_spawn(body, target_spawn)
	else:
		assert(has_node(TARGET_NAME))
		var target = get_node(TARGET_NAME)
		body.set_global_position(target.get_global_position())
	emit_signal("execution_done")

