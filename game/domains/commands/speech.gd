
extends "res://domains/component.gd"

export(String, MULTILINE) var text
export(String) var speaker_name = ""
export(bool) var disable_speaker = false
export(bool) var centered = false
export(bool) var transparent = false
export(bool) var allow_skip = true
export(bool) var auto_scroll = false
export(float) var auto_scroll_time = 0

onready var se = get_node("/root/se")

var dbox
var time

# Setup

func run(map, hud):
	dbox = hud.dialogue_box
	dbox.setup_position(centered)
	dbox.setup_opacity(transparent)
	dbox.set_allow_skip(allow_skip)
	if disable_speaker:
		dbox.unset_speaker()
	elif !speaker_name.empty():
		dbox.set_speaker(speaker_name)
	dbox.setup(text)
	time = 0
	yield(dbox, "dialogue_started")
	set_process(true)
	yield(dbox, "dialogue_ended")
	set_process(false)
	emit_signal("execution_done")


# Specials

func _ready():
	set_process(false)

func _process(dt):
	if dbox.is_state(dbox.STATE.TEXT_ANIMATION) and !auto_scroll:
		if Input.is_action_pressed('ACTION_B'):
			dbox.skip()
		elif Input.is_action_pressed('ACTION_A'):
			dbox.accelerate()
		elif Input.is_action_pressed('ACTION_C'):
			if !dbox.is_state(dbox.STATE.AWAIT_CONFIRM):
				dbox.skip()
			else:
				confirm()
	elif dbox.is_state(dbox.STATE.AWAIT_CONFIRM) and auto_scroll:
		time += dt
		if time >= auto_scroll_time:
			confirm()
	elif dbox.is_state(dbox.STATE.AWAIT_CONFIRM) and !auto_scroll:
		dbox.show_punctuation()
		if Input.is_action_just_pressed('ACTION_A') \
				or Input.is_action_just_pressed('ACTION_B') \
				or Input.is_action_pressed('ACTION_C'):
			confirm()

func confirm():
	dbox.next()
	if !auto_scroll:
		if dbox.is_buffer_empty():
			se.play("period")
		else:
			se.play("semicolon")
