
extends Object

const USER_DIR = "user://%s"

static func save_to(filename, data):
	var filepath = USER_DIR % filename
	var file = File.new()
	if file.open(filepath, File.WRITE) != 0:
		return false
	file.store_string(JSON.print(data, "  ", true))
	file.close()
	return true

static func load_from(filename):
	var filepath = USER_DIR % filename
	var file = File.new()
	var parsed_json
	if !file.file_exists(filepath):
		return false
	file.open(filepath, File.READ)
	parsed_json = JSON.parse(file.get_as_text())
	file.close()
	if parsed_json.error != 0:
		return false
	return parsed_json.result

static func delete_file(filename):
	var filepath = USER_DIR % filename
	var dir = Directory.new()
	if dir.file_exists(filepath):
		return dir.remove(filepath) == 0
	return false

