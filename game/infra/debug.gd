
extends CanvasLayer

enum { STATE_MODE = 0, PERSIST_MODE = 1 }

const TIME_UNIT = 60 * 10

const STATES_NAMES = ["State Flags", "Persist Flags"]
const BASE_MENU_LIST = ["[DONE]"]

signal debug_done

onready var gamedata = get_node("/root/profile").current_save

onready var flag_type_title = get_node("flag_type/title")
onready var flag_menu = get_node("flag_menu")
onready var flag_values_container = get_node("flag_values")
onready var flag_values = flag_values_container.get_children()

var mode = STATE_MODE
var flag_list

func _ready():
	hide()
	flag_menu.connect("pressed_A", self, "execute_action")
	flag_menu.connect("pressed_B", self, "execute_back")
	flag_menu.connect("pressed_C", self, "execute_alternate")
	flag_menu.connect("pressed_debug", self, "execute_quit")
	flag_menu.connect("selected", self, "execute_select")

func show():
	for node in get_children():
		node.show()

func hide():
	for node in get_children():
		node.hide()

func enter():
	print("DEBUG ON")
	get_tree().paused = true
	gamedata.set_process(false)
	show()
	init_flags()
	flag_menu.setup()
	flag_menu.focus()

func init_flags():
	var option_list = BASE_MENU_LIST.duplicate()
	var flags = get_current_flags()
	flag_type_title.set_text(STATES_NAMES[mode])
	flag_list = []
	for flag_name in flags.keys():
		flag_list.append(flag_name)
	flag_list.sort()
	for flag_name in flag_list:
		option_list.append(flag_name)
	flag_menu.set_list(option_list)
	flag_values_container.rect_size.y = flag_menu.options.rect_size.y
	update_flag_values(flags)

func leave():
	print("DEBUG OFF")
	gamedata.set_process(true)
	flag_menu.se.play("cancel")	
	flag_menu.unfocus()
	hide()
	yield(get_tree(), "physics_frame")
	get_tree().paused = false
	emit_signal("debug_done")

func update_flag_values(flags):
	var top = flag_menu.top
	var page_limit = flag_menu.page_limit
	for idx in range(top, top + page_limit):
		var item = flag_values[idx - top]
		if item:
			if idx == 0 or idx - 1 >= flag_list.size():
				item.set_text("")
			else:
				var flag_name = flag_list[idx - 1]
				item.set_text(String(flags[flag_name]))

func get_current_flags():
	var flags
	match mode:
		STATE_MODE:
			flags = gamedata.state
		PERSIST_MODE:
			flags = gamedata.persist
	return flags

func execute_select(idx):
	var flags = get_current_flags()
	update_flag_values(flags)

# controls

func change_flag_value(flags, flag_name, n):
	if mode == STATE_MODE and flag_name == "time":
		n *= TIME_UNIT
	flags[flag_name] += n
	update_flag_values(flags)

func execute_action(idx):
	if idx == 0:
		return execute_quit()
	flag_menu.se.play("confirm")
	var flag_name = flag_list[idx - 1]
	var flags = get_current_flags()
	change_flag_value(flags, flag_name, 1)

func execute_back(idx):
	if idx == 0:
		return
	flag_menu.se.play("confirm")
	var flag_name = flag_list[idx - 1]
	var flags = get_current_flags()
	change_flag_value(flags, flag_name, -1)

func execute_alternate(idx):
	flag_menu.se.play("toggle")
	mode = mode ^ 1
	init_flags()

func execute_quit():
	leave()
