
extends Node

onready var root = get_tree().get_root()
onready var base_size = root.get_size()
onready var profile = root.get_node("profile")

var max_aspect

func _ready():
	# setup fullscreen preferences
	var fullscreen = profile.get_preference("fullscreen")
	var aspect = profile.get_preference("aspect")
	if not aspect:
		aspect = 1
	profile.set_preference("fullscreen", not not fullscreen)
	profile.set_preference("aspect", aspect)
	fullscreen = profile.get_preference("fullscreen")
	aspect = profile.get_preference("aspect")
	update_fullscreen(fullscreen, aspect)

func _input(ev):
	if ev.is_action_pressed("META_FULLSCREEN"):
		toggle_fullscreen()
	elif ev.is_action_pressed("META_ASPECT"):
		toggle_aspect()

func set_aspect(a):
	var aspect = max(1, a)
	profile.set_preference("aspect", aspect)
	update_fullscreen(is_fullscreen(), aspect)

func get_aspect():
	return profile.get_preference("aspect")

func is_fullscreen():
	return profile.get_preference("fullscreen")

func toggle_fullscreen():
	var fullscreen = not profile.get_preference("fullscreen")
	profile.set_preference("fullscreen", fullscreen)
	update_fullscreen(fullscreen, get_aspect())

func toggle_aspect():
	var aspect = profile.get_preference("aspect")
	set_aspect(int(aspect) % get_max_aspect() + 1)

func update_fullscreen(fullscreen, aspect):
	OS.window_resizable = true
	yield(get_tree(), "idle_frame")
	OS.set_window_fullscreen(fullscreen)
	OS.request_attention()
	set_stretch_ratio(aspect)
	OS.window_resizable = false

func set_stretch_ratio(aspect):
	var screen_size = OS.get_screen_size()
	var target_size = base_size * aspect
	var margin = ((screen_size - target_size) / 2).floor()
	OS.set_window_size(target_size)
	OS.set_window_position(margin)

func get_max_aspect():
	if max_aspect:
		return max_aspect
	var screen_size = OS.get_screen_size()
	var ratio = (screen_size / base_size).floor()
	ratio.x = max(1, ratio.x)
	ratio.y = max(1, ratio.y)
	max_aspect = int(min(ratio.x, ratio.y))
	return max_aspect
