
extends ColorRect

signal fade_done

onready var tween = get_node("tween")

func fade_to(target_color, s):
	if s > 0:
		tween.interpolate_property(self, "color",
				color, target_color, s,
				Tween.TRANS_LINEAR, Tween.EASE_OUT, 0)
		tween.start()
		yield(tween, "tween_completed")
	else:
		color = target_color
	emit_signal("fade_done")

