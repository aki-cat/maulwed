
extends Node

var basepath

func _init(path):
	basepath = "res://%s/%s.tscn" % [path, "%s"]

func instance(scene):
	var full_path = basepath % scene
	print("instancing: ", full_path)
	return load(full_path).instance()

