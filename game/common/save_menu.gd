
extends Node2D

enum { SAVE_MODE, LOAD_MODE, DELETE_MODE }

const TRANSP = Color(0xffffff00)
const OPAQUE = Color(0xffffffff)
const ENTER_TIME = 0.25

# root global nodes
onready var profile = get_node("/root/profile")
onready var main = get_node("/root/main")

# local attributes
onready var menu = get_node("menu")
onready var rus = get_node("rus")
onready var tween = get_node("tween")
onready var file_info = get_node("file_info")
onready var empty = get_node("empty_slot")

var mode

func _ready():
	file_info.hide()
	empty.hide()
	set_modulate(TRANSP)

func enter(mode_requested):
	print("savemenu start! | mode: ", mode_requested)
	mode = mode_requested
	empty.hide()
	file_info.hide()
	var last_selection = profile.get_last_slot_index() + 1
	var options = ["Back"]
	for idx in range(1, profile.AVAILABLE_SLOTS + 1):
		options.append("SLOT %02d" % idx)
	menu.set_list(options)
	menu.set_selection(last_selection)
	update_selection_info(last_selection)
	tween.interpolate_method(self, "set_modulate", TRANSP, OPAQUE, ENTER_TIME,
			Tween.TRANS_LINEAR, Tween.EASE_OUT, 0)
	tween.start()
	yield(tween, "tween_completed")
	menu.connect("pressed_A", self, "execute_action")
	menu.connect("pressed_B", self, "execute_quit")
	menu.connect("pressed_C", self, "execute_alternate")
	menu.connect("pressed_pause", self, "execute_quit")
	menu.connect("selected", self, "update_selection_info")
	menu.focus()

func leave():
	menu.se.play("cancel")
	menu.unfocus()
	tween.interpolate_method(self, "set_modulate", OPAQUE, TRANSP, ENTER_TIME,
			Tween.TRANS_LINEAR, Tween.EASE_OUT, 0)
	tween.start()
	yield(tween, "tween_completed")
	queue_free()

func update_selection_info(idx):
	var info = get_slot_info(idx)
	if info:
		file_info.load_header(idx, info)
		file_info.show()
		empty.hide()
	else:
		file_info.hide()
		if idx == 0:
			empty.hide()
		else:
			empty.get_node("slot").set_text(file_info.SLOT_SIGNATURE % idx)
			empty.show()

func execute_action(idx):
	match idx:
		0:
			leave()
		_:
			var info = get_slot_info(idx)
			if info:
				# if the file is not empty
				menu.se.play("confirm")
				menu.unfocus()
				rus.focus(mode)
				var has_confirmed = yield(rus, "certainty_defined")
				if has_confirmed == rus.OPTION_NO:
					menu.focus()
					return
			elif mode == LOAD_MODE:
				# if the file is empty, but you're not saving
				menu.se.play("forbidden")
				return
			else:
				# if the file is empty and you're saving
				menu.se.play("confirm")
			var slot_idx = idx - 1
			match mode:
				SAVE_MODE:
					print("saving to slot %d" % slot_idx)
					profile.save_game_to_slot(slot_idx)
				LOAD_MODE:
					print("loading from slot %d" % slot_idx)
					profile.load_game_from_slot(slot_idx)
					main.switch_to("gameplay")
					return
			menu.focus()
			update_selection_info(idx)

func execute_quit(idx):
	leave()

func execute_alternate(idx):
	match idx:
		0:
			pass
		_:
			var info = get_slot_info(idx)
			var slot_idx = idx - 1			
			if info:
				# if the file is not empty
				menu.se.play("confirm")
				menu.unfocus()
				rus.focus(DELETE_MODE)
				var has_confirmed = yield(rus, "certainty_defined")
				if has_confirmed == rus.OPTION_NO:
					menu.focus()
					return
				print("deleting save in slot %d" % slot_idx)
				profile.delete_game_in_slot(slot_idx)
				menu.focus()
				update_selection_info(idx)

func get_slot_info(idx):
	return profile.get_slot_info(profile.SLOT_NAME_SIGNATURE % (idx - 1))
