
extends ColorRect

# making options' values very explicit
enum { OPTION_NO = 0, OPTION_YES = 1 }

const OPTION_SFX = ["cancel", "confirm"]
const MODE_VERBS = ["Overwrite", "Load", "Delete"]
const QUESTION_TEXT = "%s this slot?"

const COLOR_SELECTED = Color(0xffffffff)
const COLOR_UNSELECTED = Color(0x606060ff)

const CURSOR_BASE = Vector2(64, 48)
const CURSOR_DIST = Vector2(80, 0)

signal certainty_defined

onready var se = get_node("/root/se")

onready var question = get_node("question")
onready var cursor = get_node("cursor")
onready var options = get_node("options")

var selection = OPTION_NO

func _ready():
	unfocus()

func _input(ev):
	if ev.is_action_pressed("DIR_LEFT") or ev.is_action_pressed("DIR_RIGHT"):
		se.play("toggle")
		selection = selection ^ 1
		update_cursor()
	elif ev.is_action_pressed("ACTION_B"):
		se.play(OPTION_SFX[OPTION_NO])
		emit_signal("certainty_defined", OPTION_NO)
		unfocus()
	elif ev.is_action_pressed("ACTION_A"):
		se.play(OPTION_SFX[selection])
		emit_signal("certainty_defined", selection)
		unfocus()

func update_cursor():
	var items = options.get_children()
	items[selection].set_self_modulate(COLOR_SELECTED)
	items[selection ^ 1].set_self_modulate(COLOR_UNSELECTED)
	cursor.set_position(CURSOR_BASE + selection * CURSOR_DIST)

func focus(mode_requested):
	assert(mode_requested < MODE_VERBS.size())
	selection = OPTION_NO
	question.set_text(QUESTION_TEXT % MODE_VERBS[mode_requested])
	update_cursor()
	show()
	set_process_input(true)

func unfocus():
	hide()
	set_process_input(false)
