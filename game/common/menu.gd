
extends ColorRect

const MENU_ITEM = preload("res://common/menu_item.tscn")

const TRANSP = Color(0xffffff00)
const OPAQUE = Color(0xffffffff)
const ENTER_TIME = 0.25

const COLOR_ENABLED = Color(0xffffffff)
const COLOR_UNSELECTED = Color(0x606060ff)
const COLOR_DISABLED = Color(0xe15a40ff)
const ITEM_DIST = Vector2(0, 32)
const LH = 32

signal pressed_A
signal pressed_B
signal pressed_C
signal pressed_pause
signal pressed_debug
signal selected
signal faded_in
signal faded_out

export(bool) var remember_selection = false
export(bool) var enable_cursor = false
export(int) var vertical_margin = 16
export(int) var page_limit = 0

onready var se = get_node("/root/se")
onready var tween = get_node("tween")
onready var options = get_node("options")
onready var cursor = get_node("cursor")
onready var cursor_initial_pos = cursor.get_position()
onready var scrolls = { "above": get_node("above"),
												"below": get_node("below") }

var selection = 0
var top = 0
var item_count = 2

func _ready():
	scrolls.above.hide()
	scrolls.below.hide()
	unfocus()
	setup()

func _input(ev):
	if ev.is_action_pressed("DIR_UP"):
		select_next(-1)
	elif ev.is_action_pressed("DIR_DOWN"):
		select_next(1)
	elif ev.is_action_pressed("DIR_LEFT"):
		jump_page(-1)
	elif ev.is_action_pressed("DIR_RIGHT"):
		jump_page(1)
	elif ev.is_action_pressed("ACTION_A"):
		emit_signal("pressed_A", selection)
	elif ev.is_action_pressed("ACTION_B"):
		emit_signal("pressed_B", selection)
	elif ev.is_action_pressed("ACTION_C"):
		emit_signal("pressed_C", selection)
	elif ev.is_action_pressed("ACTION_PAUSE"):
		emit_signal("pressed_pause", selection)
	elif ev.is_action_pressed("META_DEBUG"):
		emit_signal("pressed_debug")

func jump_page(n):
	set_selection(max(0, min(item_count - 1, selection + n * page_limit)))
	update_selection()

func select_next(n):
	set_selection((selection + item_count + n) % item_count)
	update_selection()

func update_selection():
	se.play("toggle")
	emit_signal("selected", selection)

func update_cursor():
	update_top()
	var idx = 0
	for item in options.get_children():
		if idx == selection:
			item.set_self_modulate(COLOR_ENABLED)
		else:
			item.set_self_modulate(COLOR_UNSELECTED)
		idx += 1
	cursor.set_position(cursor_initial_pos + ITEM_DIST * (selection - top))

func update_top():
	if page_limit > 0:
		while selection - top >= page_limit:
			top += 1
		while selection - top < 0:
			top -= 1
		var idx = 0
		for item in options.get_children():
			item.rect_position.y = (idx - top) * LH
			idx += 1
		if top > 0:
			scrolls.above.show()
		else:
			scrolls.above.hide()
		if top < item_count - page_limit:
			scrolls.below.show()
		else:
			scrolls.below.hide()

func focus():
	if enable_cursor:
		cursor.show()
	update_cursor()
	set_process_input(true)
	
func unfocus():
	cursor.hide()
	set_process_input(false)

func enable(idx):
	var options_array = options.get_children()
	if idx >= 0 and idx < options_array.size():
		var option = options_array[idx]
		option.set_modulate(COLOR_ENABLED)

func disable(idx):
	var options_array = options.get_children()
	if idx >= 0 and idx < options_array.size():
		var option = options_array[idx]
		option.set_modulate(COLOR_DISABLED)

func get_selection():
	return selection

func set_selection(idx):
	selection = max(0, min(idx, item_count - 1))
	update_cursor()

func get_option(idx):
	return options.get_child(idx)

func setup():
	item_count = options.get_child_count()
	adjust_size()
	adjust_scroll_arrows()
	if remember_selection:
		set_selection(selection)
	else:
		set_selection(0)

func adjust_size():
	if page_limit > 0:
		options.rect_size.y = min(item_count, page_limit) * LH
		rect_size.y = vertical_margin * 2 + min(item_count, page_limit) * LH
	else:
		options.rect_size.y = item_count * LH
		rect_size.y = vertical_margin * 2 + item_count * LH

func adjust_scroll_arrows():
	var x = options.rect_position.x + options.rect_size.x * .5
	var ay = options.rect_position.y - 8
	var by = options.rect_position.y + options.rect_size.y + 8
	scrolls.above.set_position(Vector2(x, ay))
	scrolls.below.set_position(Vector2(x, by))

func set_list(options_array):
	var idx = 0
	for old_item in options.get_children():
		old_item.free()
	for item_label in options_array:
		var item = MENU_ITEM.instance()
		item.set_text(item_label)
		item.rect_position.y = idx * LH
		options.add_child(item)
		idx += 1
	setup()

func fade_in():
	tween.interpolate_property(self, "modulate", modulate, OPAQUE, ENTER_TIME,
			Tween.TRANS_LINEAR, Tween.EASE_OUT, 0)
	tween.start()
	yield(tween, "tween_completed")
	emit_signal("faded_in")

func fade_out():
	tween.interpolate_property(self, "modulate", modulate, TRANSP, ENTER_TIME,
			Tween.TRANS_LINEAR, Tween.EASE_OUT, 0)
	tween.start()
	yield(tween, "tween_completed")
	emit_signal("faded_out")

func set_faded(faded):
	if faded:
		set_modulate(TRANSP)
	else:
		set_modulate(OPAQUE)
