
extends Object

class Set:
	static func exec(a, b):
		return b

class Add:
	static func exec(a, b):
		return a + b

class Sub:
	static func exec(a, b):
		return a - b

class Mul:
	static func exec(a, b):
		return a * b

class Div:
	static func exec(a, b):
		assert(b != 0)
		return a / b

class Mod:
	static func exec(a, b):
		assert(b != 0)
		return a % b
